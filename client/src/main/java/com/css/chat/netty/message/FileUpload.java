package com.css.chat.netty.message;

import java.io.File;

/**
 * @description: 文件实体类
 * @author: zyj
 * @create: 2020-02-21 14:54
 **/
public class FileUpload {
    private String fileType;
    private String fileName;// 文件名
    //private File file;// 文件
    //private int starPos=0;// 开始位置
    private byte[] bytes;// 文件字节数组
    //private int endPos;// 结尾位置
    private String base64Str;


    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
//
//    public File getFile() {
//        return file;
//    }
//
//    public void setFile(File file) {
//        this.file = file;
//    }
//
//    public int getStarPos() {
//        return starPos;
//    }
//
//    public void setStarPos(int starPos) {
//        this.starPos = starPos;
//    }
//
//    public int getEndPos() {
//        return endPos;
//    }
//
//    public void setEndPos(int endPos) {
//        this.endPos = endPos;
//    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    public String getBase64Str() {
        return base64Str;
    }

    public void setBase64Str(String base64Str) {
        this.base64Str = base64Str;
    }

}
