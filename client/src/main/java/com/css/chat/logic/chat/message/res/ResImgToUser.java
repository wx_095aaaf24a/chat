package com.css.chat.logic.chat.message.res;

import com.css.chat.logic.chat.SendImg;
import com.css.chat.netty.message.AbstractPacket;
import com.css.chat.netty.message.FileUpload;
import com.css.chat.netty.message.PacketType;
import io.netty.buffer.ByteBuf;
import javafx.scene.image.Image;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

/**
 * @description: 发送图片
 * @author: zyj
 * @create: 2020-02-21 14:30
 **/
public class ResImgToUser extends AbstractPacket {

    private long fromUserId;
    private FileUpload fileUpload;

    public long getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(long fromUserId) {
        this.fromUserId = fromUserId;
    }

    @Override
    public void writeBody(ByteBuf buf) {
        buf.writeLong(fromUserId);
        writeFile(buf, fileUpload);
    }

    @Override
    public void readBody(ByteBuf buf) {
        this.fromUserId = buf.readLong();
        fileUpload = readFile(buf);
    }

    @Override
    public PacketType getPacketType() {
        return PacketType.RESFRIENDSENDIMG;
    }

    @Override
    public void execPacket() {
//        String str = fileUpload.getBase64Str();
//        InputStream inputStream = base64ToInputStream(str);
        InputStream inputStream = new ByteArrayInputStream(fileUpload.getBytes());
        Image image = new Image(inputStream);
        SendImg.getInstance().receiveFriendPrivateMessage(fromUserId, image);
    }
}
