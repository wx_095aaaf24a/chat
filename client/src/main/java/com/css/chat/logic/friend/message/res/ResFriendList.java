package com.css.chat.logic.friend.message.res;

import com.css.chat.logic.friend.FriendManager;
import com.css.chat.logic.friend.vo.FriendItemVo;
import com.css.chat.netty.message.AbstractPacket;
import com.css.chat.netty.message.PacketType;
import io.netty.buffer.ByteBuf;

import java.util.ArrayList;
import java.util.List;

public class ResFriendList extends AbstractPacket {

	private List<FriendItemVo> friends;

	@Override
	public PacketType getPacketType() {
		return PacketType.ResFriendList;
	}

	public List<FriendItemVo> getFriends() {
		return friends;
	}

	public void setFriends(List<FriendItemVo> friends) {
		this.friends = friends;
	}

	@Override
	public void execPacket() {
		FriendManager.getInstance().receiveFriendsList(friends);

	}

	@Override
	public void writeBody(ByteBuf buf) {
		buf.writeInt(friends.size());
		for (FriendItemVo item:friends) {
			item.writeBody(buf);
		}

	}

	@Override
	public void readBody(ByteBuf buf) {
		int size = buf.readInt();
		this.friends = new ArrayList<>(size);
		for (int i=0;i<size;i++) {
			FriendItemVo item = new FriendItemVo();
			item.readBody(buf);
			friends.add(item);
		}
	}

}
