package com.css.chat.logic.chat;

import com.css.chat.base.SessionManager;
import com.css.chat.base.UiBaseService;
import com.css.chat.logic.chat.message.req.ImgChatToUser;
import com.css.chat.logic.user.UserManager;
import com.css.chat.ui.R;
import com.css.chat.ui.StageController;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @description: 发送文件
 * @author: zyj
 * @create: 2020-02-20 10:25
 **/
public class SendImg {

    private static SendImg self = new SendImg();

    private SendImg() {}

    public static SendImg getInstance() {
        return self;
    }

    /**
     * 发送图片
     * @author zyj
     * @date 2019/8/1 15:59
     * @param friendId
     * @return
     */
    public void sendImgTo(long friendId, File file) {
        ImgChatToUser request = new ImgChatToUser(file);
        request.setToUserId(friendId);

        SessionManager.INSTANCE.sendImg(request);
    }

    public void receiveFriendPrivateMessage(long sourceId, Image message) {
        StageController stageController = UiBaseService.INSTANCE.getStageController();
        Stage stage = stageController.getStageBy(R.id.ChatToPoint);
        VBox msgContainer = (VBox)stage.getScene().getRoot().lookup("#msgContainer");

        UiBaseService.INSTANCE.runTaskInFxThread(()-> {
            Pane pane = null;
            if (sourceId != UserManager.getInstance().getMyUserId()) {
                pane = stageController.load(R.layout.PrivateChatImgRight, Pane.class);
            }else {
                pane = stageController.load(R.layout.PrivateChatImgLeft, Pane.class);
            }

            userImgShow(message, pane);
            msgContainer.getChildren().add(pane);
        });

    }


    private void userImgShow(Image message, Pane imgShow) {
        Hyperlink _nikename = (Hyperlink) imgShow.lookup("#nameUi");

        _nikename.setVisible(false);
        Label _createTime = (Label) imgShow.lookup("#timeUi");
        _createTime.setText(new SimpleDateFormat("yyyy年MM月dd日  HH:mm:ss").format(new Date()));
        Pane imagePane = (Pane) imgShow.lookup("#imagePane");
        ImageView imageView = (ImageView)imagePane.getChildren().get(0);
        imageView.setImage(message);
    }

}
