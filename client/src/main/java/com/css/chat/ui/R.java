package com.css.chat.ui;

public final class R {

	public static class id {
		public static final String RegisterView = "RegisterView";
		public static final String LoginView = "Login_View";
		public static final String MainView = "MainView";
		public static final String ChatToPoint = "ChatToPoint";
		public static final String PrivateChatItem = "PrivateChatItem";
		public static final String SearchView = "SearchView";
		public static final String FileShow = "FileShow";
	}

	public static class layout {
		/** 注册界面 */
		public static final String RegisterView = "register/xml/register.fxml";
		/** 登录界面 */
		public static final String LoginView = "login/xml/login.fxml";
		/** 主界面 */
		public static final String MainView = "main/xml/main.fxml";
		/** 主界面 */
		public static final String FriendItem = "main/xml/friendItem.fxml";
		/** 点对点聊天窗口 */
		public static final String ChatToPoint = "chat/xml/chat2Point.fxml";
		/** 点对点聊天记录 */
		public static final String PrivateChatItemLeft = "chat/xml/chatItemLeft.fxml";
		public static final String PrivateChatItemRight = "chat/xml/chatItemRight.fxml";
		/**点对点聊天左图片*/
		public static final String PrivateChatImgLeft = "chat/xml/chatImgLeft.fxml";
		/**点对点聊天右图片*/
		public static final String PrivateChatImgRight = "chat/xml/chatImgRight.fxml";

		/**文件选择器*/
		public static final String FileShow = "chat/xml/fileShow.fxml";

		public static final String SeachFriendView = "query/xml/query.fxml";
		public static final String RecommendFriendItem = "query/xml/recommend_item.fxml";
	}

}
