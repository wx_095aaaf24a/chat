package com.css.chat.ui.controller;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.bytedeco.javacv.*;
import org.bytedeco.javacv.Frame;
import org.bytedeco.opencv.opencv_core.IplImage;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

/**
 * @description:
 * @author: zyj
 * @create: 2020-02-20 13:17
 **/
public class TestController extends Application {



    @Override
    public void start(Stage primaryStage) throws Exception {

    }

    public static void main(String[] args){
        try {
            OpenCVFrameGrabber grabber = new OpenCVFrameGrabber(0);
            grabber.start();//开始获取摄像头数据

            CanvasFrame canvasFrame = new CanvasFrame("摄像头");//新建一个窗口
            while (true) {

                Frame frame = grabber.grab();
                //canvas.showImage(frame);
                OpenCVFrameConverter.ToIplImage converter = new OpenCVFrameConverter.ToIplImage();
                IplImage image = converter.convert(frame);
                BufferedImage bufferedImage = new BufferedImage(image.width(),
                        image.height(), BufferedImage.TYPE_3BYTE_BGR);
                WritableRaster raster = bufferedImage.getRaster();
                DataBufferByte dataBuffer = (DataBufferByte) raster.getDataBuffer();
                byte[] data = dataBuffer.getData();
                ((ByteBuffer) image.createBuffer()).get(data);

                ByteArrayOutputStream out = new ByteArrayOutputStream();
                try {
                    ImageIO.write(bufferedImage, "jpg", out);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                byte[] bytes =  out.toByteArray();


                Image imageh = Toolkit.getDefaultToolkit().createImage(bytes);
                try {
                    MediaTracker mt = new MediaTracker(new java.awt.Label());
                    mt.addImage(imageh, 0);
                    mt.waitForAll();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }



                //canvasFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                canvasFrame.setAlwaysOnTop(true);
                canvasFrame.showImage(imageh);//获取摄像头图像并放到窗口上显示，frame是一帧视频图像
                // 对canvas设置鼠标监听事件
                // 获取canvas
                canvasFrame.addWindowListener(new WindowAdapter() {
                    @Override
                    public void windowClosing(WindowEvent we) {
                        System.out.println("11111");
                    }
                });
                Thread.sleep(60);//50毫秒刷新一次图像
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }





}