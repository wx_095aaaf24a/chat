package com.css.chat.ui.controller;

import com.css.chat.base.UiBaseService;
import com.css.chat.logic.user.UserManager;
import com.css.chat.logic.user.model.UserModel;
import com.css.chat.ui.ControlledStage;
import com.css.chat.ui.R;
import com.css.chat.ui.ResourceContainer;
import com.css.chat.ui.StageController;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Accordion;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author zyj
 * @description
 * @date 2019/7/25 14:36
 **/
public class MainViewController implements ControlledStage, Initializable {

    @FXML
    private Accordion friends;
    @FXML
    private ScrollPane friendSp;

    @FXML
    private ImageView close;
    @FXML
    private ImageView min;
    @FXML
    private Label username;
    @FXML
    private Label signature;

    private UserModel userModel = UserManager.getInstance().getMyProfile();

    @FXML
    private void bind() {
        friendSp.setFitToWidth(false);
        friends.expandedPaneProperty().addListener(new ChangeListener<TitledPane>() {
            @Override
            public void changed(ObservableValue<? extends TitledPane> arg0, TitledPane arg1, TitledPane arg2) {
                if (arg2 != null) {
                    System.out.println("-------11111111--------");
                }
                if (arg1 != null) {
                    System.out.println("-------2222222222---------");
                }
            }
        });
    }


    @FXML
    private void min() {
        getMyStage().setIconified(true);
    }

    @FXML
    private void close() {
        System.exit(1);
    }

    @FXML
    private void closeEntered() {
        Image image = ResourceContainer.getClose_1();
        close.setImage(image);
    }

    @FXML
    private void closeExited() {
        Image image = ResourceContainer.getClose();
        close.setImage(image);
    }

    @FXML
    private void minEntered() {
        Image image = ResourceContainer.getMin_1();
        min.setImage(image);
    }

    @FXML
    private void minExited() {
        Image image = ResourceContainer.getMin();
        min.setImage(image);
    }

    @Override
    public Stage getMyStage() {
        StageController stageController = UiBaseService.INSTANCE.getStageController();
        return stageController.getStageBy(R.id.MainView);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        username.textProperty().bind(userModel.userNameProperty());
        signature.textProperty().bind(userModel.signaturePropertiy());
    }
}
