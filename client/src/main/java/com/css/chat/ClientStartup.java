package com.css.chat;

import com.css.chat.base.UiBaseService;
import com.css.chat.netty.message.PacketType;
import com.css.chat.netty.transport.SocketClient;
import com.css.chat.ui.R;
import com.css.chat.ui.StageController;
import javafx.application.Application;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * @author zyj
 * @description
 * @date 2019/7/19 15:19
 **/
public class ClientStartup extends Application {

    @Override
    public void init() throws Exception {
        PacketType.initPackets();
    }

    @Override
    public void start(Stage stage) throws Exception {
        //与服务端建立连接
        connectToServer();
        //打开界面
        openUi(stage);
    }

    private void openUi(Stage stage){
        StageController stageController = UiBaseService.INSTANCE.getStageController();
        stageController.setPrimaryStage("root", stage);

        /**1:登陆界面*/
        Stage loginStage = stageController.loadStage(R.id.LoginView, R.layout.LoginView,
                StageStyle.UNDECORATED);
        loginStage.setTitle("chat");

//        /**2 注册界面*/
//        stageController.loadStage(R.id.RegisterView, R.layout.RegisterView, StageStyle.UNDECORATED);
//
        /**3 主界面*/
        Stage mainStage = stageController.loadStage(R.id.MainView, R.layout.MainView, StageStyle.UNDECORATED);
        //把主界面放在右上方
        Screen screen = Screen.getPrimary();
        double rightTopX = screen.getVisualBounds().getWidth()*0.75;
        double rightTopY = screen.getVisualBounds().getHeight()*0.05;
        mainStage.setX(rightTopX);
        mainStage.setY(rightTopY);

        /**4: 聊天界面*/
        stageController.loadStage(R.id.ChatToPoint, R.layout.ChatToPoint, StageStyle.UNDECORATED);

        /**图片选择器*/
        stageController.loadStage(R.id.FileShow, R.layout.FileShow, StageStyle.UTILITY);

        stageController.setStage(R.id.LoginView);
    }

    private void connectToServer() {
        new Thread() {
            @Override
            public void run() {
                new SocketClient().start();
            };
        }.start();
    }

}
