package com.css;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zyj
 * @description 在项目启动后执行的功能，SpringBoot提供的一种简单的实现方案就是添加一个model并实现CommandLineRunner接口，实现功能的代码放在实现的run方法中
 * @date 2019/6/27 11:20
 *
 **/
@SpringBootApplication()
@MapperScan("com.css.dao")
public class SeverStartUpApplication{
    public static void main(String[] args) {
        SpringApplication.run(SeverStartUpApplication.class, args);
    }

}
