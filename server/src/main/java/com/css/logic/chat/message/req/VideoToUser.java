package com.css.logic.chat.message.req;

import com.css.config.base.SpringContext;
import com.css.config.netty.IoSession;
import com.css.config.netty.enums.PacketType;
import com.css.config.netty.message.AbstractPacket;
import io.netty.buffer.ByteBuf;

/**
 * @description:
 * @author: zyj
 * @create: 2020-03-04 13:58
 **/
public class VideoToUser extends AbstractPacket {

    private long toUserId;
    private byte[] videoImg;
    /**0:关闭 1：打开*/
    private int close;

    public long getToUserId() {
        return toUserId;
    }

    public void setToUserId(long toUserId) {
        this.toUserId = toUserId;
    }

    public byte[] getVideoImg() {
        return videoImg;
    }

    public void setVideoImg(byte[] videoImg) {
        this.videoImg = videoImg;
    }

    public int getClose() {
        return close;
    }

    public void setClose(int close) {
        this.close = close;
    }

    @Override
    public void readBody(ByteBuf buf) {
        this.toUserId = buf.readLong();
        this.close = buf.readInt();
        int length = buf.readInt();
        videoImg = new byte[length];
        buf.readBytes(videoImg);
    }

    @Override
    public void writeBody(ByteBuf buf) {
        buf.writeLong(this.toUserId);
        buf.writeInt(this.close);
        buf.writeInt(videoImg.length);
        buf.writeBytes(videoImg);
    }

    @Override
    public PacketType getPacketType() {
        return PacketType.REQ_VIDEO_DNL_IMG;
    }

    @Override
    public void execPacket(IoSession session) {
        SpringContext.getChatService().sendVideoImg(session, toUserId, close, videoImg);
    }
}
