package com.css.logic.chat.message.res;

import com.css.config.netty.IoSession;
import com.css.config.netty.enums.PacketType;
import com.css.config.netty.message.AbstractPacket;
import com.css.logic.chat.message.req.VideoToUser;
import io.netty.buffer.ByteBuf;

import java.awt.*;

/**
 * @description: 发送图片
 * @author: zyj
 * @create: 2020-02-21 14:30
 **/
public class ResVideoImgToUser extends AbstractPacket {

    private static ResVideoImgToUser self = new ResVideoImgToUser();

    private ResVideoImgToUser() {}

    public static ResVideoImgToUser getInstance() {
        return self;
    }
    /**0:关闭 1：打开*/
    private int close;
    private long fromUserId;
    private byte[] videoImg;

    public int getClose() {
        return close;
    }

    public void setClose(int close) {
        this.close = close;
    }

    public long getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(long fromUserId) {
        this.fromUserId = fromUserId;
    }

    public byte[] getVideoImg() {
        return videoImg;
    }

    public void setVideoImg(byte[] videoImg) {
        this.videoImg = videoImg;
    }

    @Override
    public void writeBody(ByteBuf buf) {
        buf.writeLong(fromUserId);
        buf.writeInt(close);
        buf.writeInt(videoImg.length);
        buf.writeBytes(videoImg);
    }

    @Override
    public void readBody(ByteBuf buf) {
        this.fromUserId = buf.readLong();
        this.close = buf.readInt();
        int length = buf.readInt();
        byte[] videoImg = new byte[length];
        buf.readBytes(videoImg);
    }

    @Override
    public PacketType getPacketType() {
        return PacketType.RES_VIDEO_DNL_IMG;
    }

    @Override
    public void execPacket(IoSession session) {
    }
}
