package com.css.logic.chat;

import com.css.config.netty.IoSession;
import com.css.config.netty.enums.SessionManager;
import com.css.config.netty.message.FileUpload;
import com.css.logic.chat.message.res.ResChatToUser;
import com.css.logic.chat.message.res.ResImgToUser;
import com.css.logic.chat.message.res.ResVideoImgToUser;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class ChatService {
	
	public void chat(IoSession fromUser, long toUserId, String content) {
		IoSession toUser = SessionManager.INSTANCE.getSessionBy(toUserId);
		if (fromUser == null || toUser == null) {
			return;
		}
		if (!checkDirtyWords(content)) {
			return;
		}
		
		//双方都推送消息
		ResChatToUser response = new ResChatToUser();
		response.setContent(content);
		response.setFromUserId(fromUser.getUser().getUserId());
		toUser.sendPacket(response);
		
		fromUser.sendPacket(response);
	}

	/**
	* @Description: 发送视频图片
	* @Param: [fromUser, toUserId, content]
	* @return: void
	* @Author: zyj
	* @Date: 2020/2/21
	*/
	public void sendVideoImg(IoSession fromUser, long toUserId, int close, byte[] videoImg) {
		IoSession toUser = SessionManager.INSTANCE.getSessionBy(toUserId);
		if (fromUser == null || toUser == null) {
			return;
		}

		//推送消息给好友
		ResVideoImgToUser resVideoImgToUser = ResVideoImgToUser.getInstance();
		resVideoImgToUser.setFromUserId(fromUser.getUser().getUserId());
		resVideoImgToUser.setClose(close);
		resVideoImgToUser.setVideoImg(videoImg);
		toUser.sendPacketImg(resVideoImgToUser);
	}

	/**
	 * @Description: 发送图片
	 * @Param: [fromUser, toUserId, content]
	 * @return: void
	 * @Author: zyj
	 * @Date: 2020/2/21
	 */
	public void sendImg(IoSession fromUser, long toUserId, FileUpload fileUpload) {
		IoSession toUser = SessionManager.INSTANCE.getSessionBy(toUserId);
		if (fromUser == null || toUser == null) {
			return;
		}

		//推送消息给好友
		ResImgToUser response = new ResImgToUser();
		response.setFileUpload(fileUpload);
		response.setFromUserId(fromUser.getUser().getUserId());

		toUser.sendPacketImg(response);
	}
	
	private boolean checkDirtyWords(String content) {
		return true;
	}

}
