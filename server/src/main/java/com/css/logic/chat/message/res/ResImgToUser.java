package com.css.logic.chat.message.res;


import com.css.config.netty.IoSession;
import com.css.config.netty.enums.PacketType;
import com.css.config.netty.message.AbstractPacket;
import com.css.config.netty.message.FileUpload;
import io.netty.buffer.ByteBuf;

/**
 * @description: 发送图片
 * @author: zyj
 * @create: 2020-02-21 14:30
 **/
public class ResImgToUser extends AbstractPacket {

    private long fromUserId;
    private FileUpload fileUpload;


    public long getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(long fromUserId) {
        this.fromUserId = fromUserId;
    }

    public FileUpload getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(FileUpload fileUpload) {
        this.fileUpload = fileUpload;
    }

    @Override
    public PacketType getPacketType() {
        return PacketType.RESFRIENDSENDIMG;
    }

    @Override
    public void execPacket(IoSession session) {

    }

    @Override
    public void writeBody(ByteBuf buf) {
        buf.writeLong(fromUserId);
        writeFile(buf, fileUpload);
    }

    @Override
    public void readBody(ByteBuf buf) {
        this.fromUserId = buf.readLong();
        fileUpload = readFile(buf);
    }
}
