package com.css.logic.friend.message.res;

import com.css.config.netty.IoSession;
import com.css.config.netty.enums.PacketType;
import com.css.config.netty.message.AbstractPacket;
import io.netty.buffer.ByteBuf;

/**
 * 好友注销
 * @author
 */
public class ResFriendLogout extends AbstractPacket {

	private long friendId;

	public long getFriendId() {
		return friendId;
	}

	public void setFriendId(long friendId) {
		this.friendId = friendId;
	}

	@Override
	public PacketType getPacketType() {
		return PacketType.ResFriendLogout;
	}

	@Override
	public void execPacket(IoSession session) {
		// TODO Auto-generated method stub

	}

	@Override
	public void writeBody(ByteBuf buf) {
		buf.writeLong(friendId);
	}

	@Override
	public void readBody(ByteBuf buf) {
		this.friendId = buf.readLong();
	}

}
