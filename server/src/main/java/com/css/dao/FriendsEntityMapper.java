package com.css.dao;

import com.css.entity.FriendsEntity;
import com.css.util.model.FriendView;

import java.util.List;

public interface FriendsEntityMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(FriendsEntity record);

    int insertSelective(FriendsEntity record);

    FriendsEntity selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(FriendsEntity record);

    int updateByPrimaryKey(FriendsEntity record);

    List<FriendView> getMyFriends(Long userId);
}