package com.css.config.netty;

import com.css.config.netty.enums.SessionCloseReason;
import com.css.config.netty.message.AbstractPacket;
import com.css.config.netty.message.FileUpload;
import com.css.logic.chat.message.req.ImgChatToUser;
import com.css.util.model.User;
import com.css.util.netty.ChannelUtils;
import io.netty.channel.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 链接的会话
 * @author
 */
public class IoSession {

	private static final Logger logger = LoggerFactory.getLogger(IoSession.class);

	private volatile int lastLength = 0;
	private int byteRead;

	/** distributeKey auto generator  */
	private AtomicInteger dispatchKeyGenerator = new AtomicInteger();

	/** 网络连接channel */
	private Channel channel;

	private User user;

	/** ip地址 */
	private String ipAddr;

	private boolean reconnected;

	/** 业务分发索引 */
	private int dispatchKey;

	/** 拓展用，保存一些个人数据  */
	private Map<String, Object> attrs = new HashMap<>();



	public IoSession() {

	}

	public IoSession(Channel channel) {
		this.channel = channel;
		this.ipAddr = ChannelUtils.getIp(channel);
		this.dispatchKey = dispatchKeyGenerator.getAndIncrement();
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Channel getChannel() {
		return channel;
	}

	public int getDispatchKey() {
		return dispatchKey;
	}



	public String getIpAddr() {
		return ipAddr;
	}

	public void setIpAddr(String ipAddr) {
		this.ipAddr = ipAddr;
	}

	public boolean isReconnected() {
		return reconnected;
	}

	public void setReconnected(boolean reconnected) {
		this.reconnected = reconnected;
	}

	public User getUser() {
		return user;
	}

	public boolean isClose() {
		if (channel == null) {
			return true;
		}
		return !channel.isActive() ||
			   !channel.isOpen();
	}

	/**
	 * 向客户端发送消息
	 * @param packet
	 */
	public void sendPacket(AbstractPacket packet) {
		if (packet == null) {
			return;
		}
		if (channel != null) {
			channel.writeAndFlush(packet);
		}
	}

	/**
	 * 向客户端发送图片
	 * @param packet
	 */
	public void sendPacketImg(AbstractPacket packet) {
		if (packet == null) {
			return;
		}
		if (channel != null) {
			channel.writeAndFlush(packet);
		}
//		try {
//			if( !(packet instanceof ImgChatToUser)){
//				logger.error("文件类型不正确,应为img");
//			}
//			ImgChatToUser imgChatToUser = (ImgChatToUser) packet;
//			FileUpload fileUpload = imgChatToUser.getFileUpload();
//
//			RandomAccessFile randomAccessFile = new RandomAccessFile(fileUpload.getFile(), "r");
//			randomAccessFile.seek(fileUpload.getStarPos());
//			// lastLength = (int) randomAccessFile.length() / 10;
//			lastLength = 1024 * 10;
//			byte[] bytes = new byte[lastLength];
//			if ((byteRead = randomAccessFile.read(bytes)) != -1) {
//				fileUpload.setEndPos(byteRead);
//				fileUpload.setBytes(bytes);
//				imgChatToUser.setFileUpload(fileUpload);
//				channel.writeAndFlush(imgChatToUser);//发送消息到服务端
//			} else {
//			}
//			logger.info("channelActive()文件已经读完 {}", byteRead);
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException i) {
//			i.printStackTrace();
//		}
//		logger.info("channelActive()方法执行结束");
	}

	/**
	 * 关闭session
	 * @param reason {@link SessionCloseReason}
	 */
	public void close(SessionCloseReason reason) {
		try{
			if (this.channel == null) {
				return;
			}
			if (channel.isOpen()) {
				channel.close();
				logger.info("close session[{}], reason is {}", getUser().getUserId(), reason);
			}else{
				logger.info("session[{}] already close, reason is {}", getUser().getUserId(), reason);
			}
		}catch(Exception e){
		}
	}

}
