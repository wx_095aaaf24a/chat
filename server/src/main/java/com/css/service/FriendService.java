package com.css.service;

import com.css.config.base.Constants;
import com.css.config.netty.enums.SessionManager;
import com.css.dao.FriendsEntityMapper;
import com.css.logic.friend.message.res.ResFriendList;
import com.css.logic.friend.message.res.ResFriendLogin;
import com.css.logic.friend.message.res.ResFriendLogout;
import com.css.util.model.FriendView;
import com.css.util.model.User;
import com.css.util.vo.FriendItemVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zyj
 * @description
 * @date 2019/7/19 11:42
 **/
@Service
public class FriendService {
    @Autowired
    private UserService userService;
    @Autowired
    private FriendsEntityMapper friendDao;

    public void onUserLogout(long userId) {
        List<FriendItemVo> myFriends = listMyFriends(userId);
        ResFriendLogout logoutPact = new ResFriendLogout();
        logoutPact.setFriendId(userId);
        for (FriendItemVo friend:myFriends) {
            long friendId = friend.getUserId();
            if (userService.isOnlineUser(friendId)) {
                SessionManager.INSTANCE.sendPacketTo(friendId, logoutPact);
            }
        }
    }

    public List<FriendItemVo> listMyFriends(long userId) {
        List<FriendItemVo> result = new ArrayList<>();
        FriendItemVo friendItemVo = new FriendItemVo();
        friendItemVo.setUserName("111");
        friendItemVo.setUserId(1111L);
        result.add(friendItemVo);
        List<FriendView> friends = friendDao.getMyFriends(userId);
        for (FriendView f:friends) {
            if (f.getUserId() == userId) {
                continue;
            }
            FriendItemVo item = new FriendItemVo();
            item.setGroup(f.getGroup());
            item.setRemark(f.getRemark());
            item.setSex(f.getSex());
            item.setSignature(f.getSignature());
            item.setUserId(f.getUserId());
            item.setUserName(f.getUserName());
            item.setGroupName(f.getGroupName());
            if (userService.isOnlineUser(f.getUserId())) {
                item.setOnline(Constants.ONLINE_STATUS);
            }

            result.add(item);
        }

        return result;
    }

    public void refreshUserFriends(User user) {
        List<FriendItemVo> myFriends = listMyFriends(user.getUserId());
        ResFriendList friendsPact = new ResFriendList();
        friendsPact.setFriends(myFriends);

        SessionManager.INSTANCE.sendPacketTo(user, friendsPact);

        onUserLogin(user);
    }

    public void onUserLogin(User user) {
        List<FriendItemVo> myFriends = listMyFriends(user.getUserId());
        ResFriendLogin loginPact = new ResFriendLogin();
        loginPact.setFriendId(user.getUserId());
        for (FriendItemVo friend:myFriends) {
            long friendId = friend.getUserId();
            if (userService.isOnlineUser(friendId)) {
                SessionManager.INSTANCE.sendPacketTo(friendId, loginPact);
            }
        }
    }
}
