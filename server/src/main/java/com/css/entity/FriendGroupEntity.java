package com.css.entity;

public class FriendGroupEntity {
    private Long id;

    private String name;

    private Long userid;

    public FriendGroupEntity(Long id, String name, Long userid) {
        this.id = id;
        this.name = name;
        this.userid = userid;
    }

    public FriendGroupEntity() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }
}