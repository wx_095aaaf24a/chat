package com.css.entity;

public class FriendsEntity {
    private Integer id;

    private Long userid;

    private Long friendid;

    private String remark;

    private Long groupid;

    public FriendsEntity(Integer id, Long userid, Long friendid, String remark, Long groupid) {
        this.id = id;
        this.userid = userid;
        this.friendid = friendid;
        this.remark = remark;
        this.groupid = groupid;
    }

    public FriendsEntity() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public Long getFriendid() {
        return friendid;
    }

    public void setFriendid(Long friendid) {
        this.friendid = friendid;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getGroupid() {
        return groupid;
    }

    public void setGroupid(Long groupid) {
        this.groupid = groupid;
    }
}